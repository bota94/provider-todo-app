import 'package:flutter/material.dart';
import 'package:provider_todo/Databases/tasks_db.dart';
import 'package:provider_todo/Models/Task.dart';

class TasksProvider with ChangeNotifier {
  List<Task> _tasks = [];
    TasksDataBase db = TasksDataBase.instance;


  List<Task> get tasks {
    db.readAllTasks().then((value) {
      _tasks = value;
      return _tasks;
    });
    return _tasks;
  }


  void createTask(String title, DateTime deadline) {
    Task newTask = Task(
      title: title,
      done: false,
      deadline: deadline,
    );
    _tasks.add(newTask);
    db.create(newTask);
    notifyListeners();
  }

  void toggleTaskState(Task task) async{
    task.done = !task.done;
    // await db.update(task);
    notifyListeners();
  }
}
