import 'package:provider_todo/Models/Task.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';

class TasksDataBase {
  static final TasksDataBase instance = TasksDataBase._init();
  static Database? _database;
  TasksDataBase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB('tasks.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const boolType = 'BOOLEAN NOT NULL';
    const textType = 'TEXT NOT NULL';

    await db.execute(
      '''
        CREATE TABLE $tasksTable (
          ${TaskFields.id} $idType,
          ${TaskFields.title} $textType,
          ${TaskFields.done} $boolType,
          ${TaskFields.deadline} $textType
        )
      '''
    );
  }

  Future<Task> create(Task task) async {
    final db = await instance.database;
    final id = await db.insert(tasksTable, task.toJson());
    return task.copy(id: id);
  }

  Future<List<Task>> readAllTasks() async {
    final db = await instance.database;
    final maps = await db.query(tasksTable,
        columns: TaskFields.values);
    List<Task> tasks = maps.map((json) => Task.fromJson(json)).toList();
    return maps.map((json) => Task.fromJson(json)).toList();
  }

  Future<int> update(Task task) async {
    final db = await instance.database;
    return db.update(
      tasksTable,
      task.toJson(),
      where: '${TaskFields.id} = ?',
      whereArgs: [task.id],
    );
  }


}
