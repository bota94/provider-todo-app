// ignore_for_file: file_names
const String tasksTable = 'tasks';

class TaskFields {
  static const String id = '_id';
  static const String title = 'title';
  static const String done = 'done';
  static const String deadline = 'deadline';

  static final List<String> values = [
    id,
    title,
    done,
    deadline,
  ];
}

class Task {
  final int? id;
  String title;
  bool done;
  DateTime deadline;

  Task(
      {this.id,
      required this.title,
      required this.done,
      required this.deadline});

  static Task fromJson(Map<String, Object?> json) => Task(
        id: json[TaskFields.id] as int,
        title: json[TaskFields.title] as String,
        done: json[TaskFields.done] == 1,
        deadline: DateTime.parse(json[TaskFields.deadline] as String),
      );

  Map<String, Object?> toJson() => {
        TaskFields.id: id,
        TaskFields.done: done ? 1 : 0,
        TaskFields.title: title,
        TaskFields.deadline: deadline.toIso8601String(),
      };

  Task copy({
    int? id,
    String? title,
    bool? done,
    DateTime? deadline,
  }) =>
      Task(
        id: id ?? this.id,
        title: title ?? this.title,
        done: done ?? this.done,
        deadline: deadline ?? this.deadline,
      );
}
