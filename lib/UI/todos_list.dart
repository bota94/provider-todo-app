import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider_todo/UI/task_item.dart';
import 'package:provider_todo/providers/tasks_provider.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class TodosList extends StatefulWidget {
  const TodosList({Key? key}) : super(key: key);

  @override
  State<TodosList> createState() => _TodosListState();
}

class _TodosListState extends State<TodosList> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _taskController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: InkWell(
        onTap: () {
          showModalBottomSheet<void>(
              context: context,
              isScrollControlled: true,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              builder: (BuildContext context) {
                return _buildBottomSheet();
              });
        },
        child: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.lightBlue[700],
            ),
            child: const Icon(Icons.add, color: Colors.white, size: 28)),
      ),
      backgroundColor: Colors.lightBlue[900],
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.lightBlue[900],
        // leading: const Icon(Icons.menu, color: Colors.white),
        automaticallyImplyLeading: false,
        title: const Text(
          "Todo Application",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Center(
              child: Text(
                DateFormat.jm().format(DateTime.now()).toString(),
                style: const TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(25),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50),
                  topRight: Radius.circular(50),
                ),
              ),
              child: ListView.separated(
                itemBuilder: (context, index) =>
                    TaskItem(task: context.watch<TasksProvider>().tasks[index]),
                separatorBuilder: (context, _) => const Divider(),
                itemCount: context.watch<TasksProvider>().tasks.length,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildBottomSheet() {
    DateTime deadline = DateTime.now();
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white24,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        height: 300,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Text('Add new todo',
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                TextFormField(
                  controller: _titleController,
                  decoration: const InputDecoration(hintText: "Title"),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text("Deadline"),
                    Container(
                      height: 70,
                      width: 400,
                      margin: const EdgeInsets.only(top: 20),
                      child: CupertinoDatePicker(
                        mode: CupertinoDatePickerMode.date,
                        initialDateTime: DateTime.now(),
                        // minimumDate: DateTime.now(),
                        onDateTimeChanged: (DateTime newDateTime) {
                          deadline = newDateTime;
                        },
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: ElevatedButton(
                      child: const Text('Add Task'),
                      style: ElevatedButton.styleFrom(
                          fixedSize: const Size(200, 50)),
                      onPressed: () {
                        if (_titleController.text != "") {
                          context.read<TasksProvider>().createTask(
                              _titleController.text, deadline);
                          Navigator.pop(context);
                          _titleController.clear();
                          _taskController.clear();
                        }
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
