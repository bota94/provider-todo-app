import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:provider_todo/Models/Task.dart';
import 'package:provider_todo/providers/tasks_provider.dart';

class TaskItem extends StatelessWidget {
  final Task task;
  const TaskItem({Key? key, required this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              task.title,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  decoration: task.done
                      ? TextDecoration.lineThrough
                      : TextDecoration.none),
            ),
            Text(
              DateFormat.yMEd().format(task.deadline).toString(),
            )
          ],
        ),
        InkWell(
          onTap: () {
            context.read<TasksProvider>().toggleTaskState(task);
          },
          child: Container(
            width: 35,
            height: 35,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: task.done ? Colors.yellow : Colors.white,
              border: Border.all(color: Colors.yellow, width: 3),
            ),
            child: const Icon(
              Icons.check,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
